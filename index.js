const express = require("express")
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/user");
const courseRoutes = require("./routes/course");

const app = express();

// Connect to mongo MongoDB
mongoose.connect("mongodb+srv://dbkimbryanvilloga:c6LHvb7Nvt7Saca9@wdc028-course-booking.pypbc.mongodb.net/myFirstDatabase?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true,
	}
);

mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas'))

app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Defines the "users" string to be included for all user routes define in the "user" route file
app.use("/users", userRoutes);

// Defines the "/courses" string to be included for all course routes defined in the course routes
app.use("/courses", courseRoutes);

// Listen to port
app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${ process.env.PORT || 4000 }`)
});
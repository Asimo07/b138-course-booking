const Course = require("../model/Course");

// Create a new course
module.exports.addCourse = async (user, reqBody) => {

	if(user.isAdmin){
	// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
		let newCourse = new Course({
			name : reqBody.name,
			description : reqBody.description,
			price : reqBody.price
		});

		// Save the created object to the database
		 return newCourse.save().then((course, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		});
	}
	else{
		return (`You have no access`);
	}
}

// Controller method for retrieving all the courses
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	})
}

// Retrieve all active courses
module.exports.getAllActive = () => {
	return Course.find({isActive : true}).then(result => {
		return result;
	})
}

// Retrive a speicific course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	})
}

// Update a course
module.exports.updateCourse = async (user, reqParams, reqBody) => {
	// Validate if user is Admin
	if(user.isAdmin){
			// Specify the fields/property of the document to be updated
			let updatedCourse = {
				name : reqBody.name,
				description : reqBody.description,
				price : reqBody.price
			}

			return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
				if(error){
					return false;
				}
				else{
					return true;
				}
			})
	}else{
		return (`You have no access`)
	}
	
}

module.exports.archieve = async (user, reqParams, reqBody) => {

	if(user.isAdmin){
		let status = {
			isActive: reqBody.isActive
		}

		return Course.findByIdAndUpdate(reqParams.courseId, status).then((course, error) => {
				if(error){
					return false;
				}
				else{
					return true;
				}
			})
	}else{
		return (`You have no access`)
	}
	
}
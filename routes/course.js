const express = require("express");
const router = express.Router();
const courseController = require("../controllers/course");
const auth = require("../auth")

// Route for creating a course
router.post("/", auth.verify, (req, res) => {

	const data = auth.decode(req.headers.authorization);
	courseController.addCourse(data, req.body).then(resultFromController => res.send(resultFromController));
});

// Route for retrieving all the courses
router.get("/all", (req, res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
})

// Route for retrieving all active courses
router.get("/", (req, res) => {
	courseController.getAllActive().then(resultFromController => res.send(
		resultFromController));
})

// Route for retreiving a specific course
router.get("/:courseId", (req, res) => {
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
})

// Route for updating a course
router.put("/:courseId", auth.verify, (req, res) => {

	const user = auth.decode(req.headers.authorization);
	courseController.updateCourse(user, req.params, req.body).then(resultFromController => res.send(resultFromController));
})

router.put("/:courseId/archieve", auth.verify, (req, res) => {

	const user = auth.decode(req.headers.authorization);
	courseController.archieve(user, req.params, req.body).then(resultFromController => res.send(resultFromController));
})



module.exports = router;